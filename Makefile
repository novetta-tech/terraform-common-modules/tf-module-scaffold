.SHELLFLAGS = -ec

MODULE_DIR ?= cd src;
TEST_DIR ?= cd test;
SCRIPTS_DIR ?= cd scripts;

.PHONY: clean init format lint test scan gendocs build
all: clean init format lint test scan gendocs build

init:
	@ $(MODULE_DIR) terraform init

format:
	@$(MODULE_DIR) terraform fmt -recursive .

lint:
	@$(SCRIPTS_DIR) shellcheck ./*.sh
	@$(MODULE_DIR) terraform fmt -check -diff .
	@$(MODULE_DIR) tflint --format=checkstyle .

test:
	@$(TEST_DIR) go test -v 

scan:
	@$(MODULE_DIR) tfsec .
	@$(MODULE_DIR) terraform validate .

gendocs:
	@cp README.md .README.old
	terraform-docs markdown src > .module_info.md
	@echo "## Developing" >> .module_info.md
	@sed -i -e '/\#\# Providers/,/\#\# Developing/c->gen_doc_auto_insert_here<-' README.md
	@sed -i -e '/->gen_doc_auto_insert_here<-/{r .module_info.md' -e 'd}' README.md
	@rm .module_info.md
	@rm .README.old

build: clean
	@mkdir -p dist
	@$(MODULE_DIR) tar -zcvf ../dist/module.tar.gz .
	@$(MODULE_DIR) zip -r ../dist/module.tar.gz .

clean:
	@rm .*.old || true
	@rm -rf src/.terraform || true
	@rm -rf dist || true